import numpy as np

def shannonEntropy(msa):
    """ Compute the Shannon entropy for each column of the MSA independently.

    Keyword arguments:
         msa (ndarray): A numpy array containing the MSA in numerical format

    Returns:
         S (ndarray) : A 1-dimensional ndarray containing the entropies of all the MSA columns
    """

    B,N=msa.shape
    S = np.zeros(N)

    for i in np.arange(N):
        pc,bc = np.histogram(msa[:,i],bins=np.arange(-.5,21))
        p,b = np.histogram(msa[:,i],bins=np.arange(-.5,21),density=True)
        
        Sp = -p*np.log(p)
        Sp[pc==0]=0
        S[i]= Sp.sum()/np.log(21)

    return S
        

def maxConservation(msa):
    """ Compute the maximum conservation for each column of the MSA independently. 
        The maximum conservation is defined as the fraction of the most appearing amino-acid in the column

    Keyword arguments:
         msa (ndarray): A numpy array containing the MSA in numerical format

    Returns:
         maxConservation (ndarray) : A 1-dimensional ndarray containing the maximum conservations of all the MSA columns
    """

    B,N=msa.shape
    maxConservation = np.zeros(N)

    for i in np.arange(N):
        pc,bc = np.histogram(msa[:,i],bins=np.arange(-.5,21),density=True)
        maxConservation[i]=pc.max()

    return maxConservation

import os
import sys
import Bio.PDB
import Bio.SeqIO
import copy
import Bio.Seq
import subprocess
import numpy as np
from Bio import pairwise2
import scipy.spatial.distance
from sklearn.preprocessing import OneHotEncoder

aaDict={'-':0, 'X':0,'Z':0,'B':0,'*':0, 'A':1, 'C':2, 'D':3, 'E':4, 'F':5, 'G':6, 'H':7, 'I':8,'K':9,
        'L':10, 'M':11,'N':12, 'P':13, 'Q':14, 'R':15, 'S':16, 'T':17, 'V':18, 'W':19, 'Y':20}

invAaDict={0:'-', 1:'A', 2:'C', 3:'D', 4:'E', 5:'F', 6:'G', 7:'H', 8:'I', 9:'K', 10:'L',
        11:'M', 12:'N', 13:'P', 14:'Q', 15:'R', 16:'S', 17:'T', 18:'V', 19:'W', 20:'Y'}

def alignSequenceToHMM(sequence,hmmFile):
    """ Aligns a sequence to a hmmer-generated HMM.

    Keyword Arguments:
        sequence (str): The sequence to be alignd as a simple string.

        hmmFile (str): The family hmm file.


    Returns:
        mapIndexes (ndarray): 1-D array containing the mapping indexes between the sequence and the hmmFile. 
                              The array is such that mapIndexes[i] maps sequence[i] to its position in the hmm.
    """
    
    # Align the pdbSequence to the HMM
    Bio.SeqIO.write(Bio.SeqRecord.SeqRecord(Bio.Seq.Seq(sequence),'tmp','tmp'),'__tmpFasta.fasta','fasta')
    out=subprocess.check_output(["hmmalign",hmmFile,'__tmpFasta.fasta']).decode('utf-8').splitlines()
    os.remove('__tmpFasta.fasta')
    rawMap=''
    gapMap=''
    for l in out:
        if l.startswith("#=GC RF"):
            rawMap+=l.split()[2]
        if l.startswith("#=GC PP_cons"):
            gapMap+=l.split()[2]

    mapIndexes=[]
    aaCounter=0
    for idx,c in enumerate(rawMap):
        if c == "x" and gapMap[idx] != '.' :
            mapIndexes.append(aaCounter)
            aaCounter+=1
        elif c=="x" and gapMap[idx] == '.':
            mapIndexes.append(-sys.maxsize)
        elif c==".":
            aaCounter+=1

    return np.asarray(mapIndexes)

def computeDistanceMap(pdbFile,chainIds,hmmFiles=[None]):
  """ Maps a PDB structure to a hmm to compare DCA predictions with structural contacts. 
      This can be used either to only compute the PDB distance map (default, with hmmFiles=[None]) 
      or used to futher aligne the distance map to the hmm models.

  Keyword arguments:
      pdbFile (str)  : The structure file to map in PDB format 
    
      chainIds (list[chars]) : The chain Id(s) of the chain(s) to be mapped.

      hmmFiles (list[str]) : Optional hmm files onto which the pdb map is aligned.
 
  Return:
      map (ndarray) : A NxN array containing the (possibly hmm-aligned) distance map.
  """

  align=True
  collapse=False
  if len(hmmFiles)!=1 and len(hmmFiles)!=len(chainIds):
    raise RuntimeError("You need to provide one hmm file for every chain id OR only one hmm file to be used for every chainId.")
  if len(hmmFiles)==1:
    hmmfile=hmmFiles[0]
    if len(chainIds)>1:
      collapse=True
    if hmmfile is None:
      align=False
    hmmFiles=[hmmfile for idx in chainIds]
  
  chainIds.sort()
  
  # Parse the PDB and extract the residues of the target chains
  structformat=pdbFile.split('.')[-1]
  if structformat=="pdb":
    structure = Bio.PDB.PDBParser().get_structure('void', pdbFile)
  elif structformat=="cif":
    structure = Bio.PDB.MMCIFParser().get_structure('void', pdbFile)
  chains = structure[0].get_chains()
  struct_chids=frozenset([chain.id for chain in chains])
  for idx in chainIds:
    if idx not in struct_chids:
      raise RuntimeError("Cannot find chain id {} in pdb {}. Available chains: {}".format(idx,pdbFile,",".join(struct_chids)))

  chains = structure[0].get_chains()
  chains=sorted(chains,key=lambda ch: ch.id)
  
  residues=[chain.get_list() for chain in chains if chain.id in chainIds]
  residues = [res for resList in residues for res in resList]
  # Remove waters and hetero_residues
  residues = [res for res in residues if (res.id[0][0] not in ["W", "H"])]
  
  # Compute the distance map between all residues (minimal distance between atoms of the chain)
  distanceMap=np.zeros([len(residues),len(residues)],dtype=float)
  catPdbSeq=''
  for idx1,res1 in enumerate(residues):
    atoms1 = np.array([r.get_coord() for r in res1.get_atoms()])
    catPdbSeq+=Bio.PDB.Polypeptide.three_to_one(res1.resname)
    for idx2,res2 in enumerate(residues):
      if idx2>idx1:
        atoms2 = np.array([r.get_coord() for r in res2.get_atoms()])
        atomDist = scipy.spatial.distance.cdist(atoms1,atoms2)
        distanceMap[idx1,idx2]=atomDist.min()
        distanceMap[idx2,idx1]=distanceMap[idx1,idx2]
  
  # Align the pdb sequences to the HMMs
  if align:
    pdbSeqs=[]
    currentLength=0
    for chainId in chainIds:
      for chain in structure[0].get_chains():
        if chain.id==chainId:
          d=len([res for res in chain.get_list() if res.id[0][0] not in ["W","H"]])
          pdbSeqs.append(catPdbSeq[currentLength:(currentLength+d)])
          currentLength+=d

    mapIndexes=np.array([],dtype=int)
    offset=0
    for i in range(len(chainIds)):
      mapIndexes=np.concatenate([mapIndexes,offset+np.array(alignSequenceToHMM(pdbSeqs[i],hmmFiles[i]))])
      offset+=len(pdbSeqs[i])
      
    # Align the PDB distance map onto the HMM
    mi=mapIndexes
    dm=np.zeros([len(mi), len(mi)])
    dm[:]=np.inf
    dm[np.ix_(mi>=0,mi>=0)]=distanceMap[np.ix_(mi[mi>=0],mi[mi>=0])]  
  else:
    dm=distanceMap
  
  # For homo-multimeric mappings, reduce the distance map to a NxN map containing also the homo-multimeric contacts.
  # The lower triangular part will contain intra-chain interactions only while the upper triangular part will also contain inter-chain interactions
  if collapse:
    size=dm.shape[0]//len(chainIds)
    distanceMap=np.zeros((size,size))
    dmc=np.zeros((size,size))
    dmc[:]=np.inf
    # collapse diagonal blocks
    for i in range(len(chainIds)):
      dmc=np.minimum(dmc,dm[i*size:(i+1)*size,i*size:(i+1)*size])
    i_lower=np.tril_indices(size)
    distanceMap[i_lower]=dmc[i_lower]
    # collapse off-diagonal blocks
    for i in range(len(chainIds)-1):
      for j in range(i+1,len(chainIds)):
        dmc=np.minimum(dmc,dm[i*size:(i+1)*size,j*size:(j+1)*size])
        dmc=np.minimum(dmc,dm[j*size:(j+1)*size,i*size:(i+1)*size].T)
    dmc=np.minimum(dmc,dmc.T)
    i_upper=np.triu_indices(size)
    distanceMap[i_upper]=dmc[i_upper]
    dm=distanceMap
  return dm


def pdbToFasta(pdbFile,chainID):
  """ Extracts the sequence of a PBD file and saves it to fasta format

  Keyword Arguments:
      pdbFile (str): Filename of PDB structure
      
      chainID (str): Chain ID in the PDB

      fastaFile(str): Name of the output fasta file 
  """
    
  structure = Bio.PDB.PDBParser().get_structure('void', pdbFile)
  chains = structure[0].get_chains()

  residues=[chain.get_list() for chain in chains if chain.id== chainID]
  residues = [res for resList in residues for res in resList]

  for i,r in enumerate(residues[:]):
    if r.id[0]=='W':
      residues.remove(r)
    elif r.id[0]=='H_MSE':
      residues[i].id=('',r.id[1],'')
      residues[i].resname='MET'
    elif len(r.id[0])>2 and r.id[0][:2]=='H_':
      residues.remove(r)

  residues = [res.resname for res in residues ]
  residues_321 = []
  for r in residues:
    try:
      one=Bio.PDB.Polypeptide.three_to_one(r)
    except:
      one='-'
    residues_321.append(one)
  sequence = "".join(residues_321)

  return sequence





















def binarizeMSA(msa):
    """ Expands the categorical MSA in extended binary form.

    Keyword Arguments:
            msa (ndarray): A numpy array containing the MSA in numerical format

    Returns:
        binaryMSA (scipy.sparse.csr): A 2D scipy.sparse.csr.csr_matrix of dimensions (B,21*N), 
                                      where B is the number of sequences in the MSA and N
                                      the original width of the MSA.
    """
   
    enc=OneHotEncoder(categories=[range(21) for n in range(msa.shape[1])])
    binaryMSA=enc.fit_transform(msa)
    return binaryMSA




def splitSpeciesMSA(fullMSA,lineage,taxaQuery,speciesMSA):
    """ Extracts all sequences from a specific species from an MSA
    
    Keyword Arguments:
        fullMSA (str): Complete MSA in fasta format.

        lineage (str): Lineage file. Each line corresponds to a sequence in fullMSA in the same order. 

        taxaQuery (str): The taxonomic query. All sequences belonging to the query are returned in the output speciesMSA.

        speciesMSA (str): The output MSA file name containing only sequences of fullMSA belonging to the taxa query.
    """

    msa=Bio.SeqIO.parse(fullMSA,'fasta')
    B=len(list(msa))
    
    with open(lineage,'r') as f:
        lineageList=list(f)
    lineageList=[[ll.lstrip().rstrip() for ll in l.split('\t')] for l in lineageList]

    if len(list(lineageList)) != B:
        logging.error("Lineage file lenght inconsistent with MSA length.")
        sys.exit()


    msa=Bio.SeqIO.parse(fullMSA,'fasta')
    taxaHits=[]
    for idx,seq in enumerate(msa):
        if seq.id.split('_')[0].split('.')[0].split('/')[0]!=lineageList[idx][0]:
            # seq.id.split('|')[1]!=lineageList[idx][0]:
            logging.error("Inconsistent lineage file; seqId: {}".format(seq.id))
            sys.exit()

        if taxaQuery.rstrip().lstrip() in lineageList[idx]:
            taxaHits.append(seq)

    Bio.SeqIO.write(taxaHits,speciesMSA,'fasta')


def makeTaxaTags(fullMSA,lineage,taxaQueries,tagFile):
    """ Tags all sequences in fullMSA with specific  taxa queries.

    Keyword Arguments:
        fullMSA (str): Complete MSA in fasta format.

        lineage (str): Lineage file. Each line corresponds to a sequence in fullMSA in the same order. 

        taxaQueries (list): List of the taxonomic queries.

        tagFile (str): The output file containing the numeric tags associated to the taxaQueries.
    """
    
    msa=sys.argv[1]
    lineageFile=sys.argv[2]
    taxaQueries=[]
    queries=sys.argv[3:]
    
    with open(lineageFile,'r') as f:
        lineage=list(f)
        
    lineage=[l.split('\t') for l in lineage]
    lineage={l[0] : l for l in lineage}

    msa=Bio.SeqIO.parse(msa,'fasta')
    genericTag=0
    fout=open(tagFile,'w')
    fout.write("Taxa\tHeader\n")
    for seq in msa:
        id=seq.id.split('|')[1]
        tag=genericTag
        for idx,query in enumerate(queries):
            if query in lineage[id]:
                tag=idx+1
                break
        if tag != genericTag:
            fout.write(seq.id+'\t'+str(tag) + "\n")
    fout.close()




def mapPDBToSequence(pdbFile, chainId, sequence1, seq1Mapping,mapFile):
    """ Maps a PDB structure to a reference mapped structure. 

    Keyword arguments:

    Return:
        map (ndarray) : A NxN array containing the (possibly mapped) distance map.

   """

    ### Compute the unaligned distance map
    # Parse the PDB and extract the residues of the target chains
    structure = Bio.PDB.PDBParser().get_structure('void', pdbFile)
    
    chains = structure[0].get_chains()

    residues=[chain.get_list() for chain in chains if chain.id== chainId]
    residues = [res for resList in residues for res in resList]

    # Remove waters and hetero_residues
    residues = [res for res in residues if (res.id[0][0] not in ["W", "H"])]

    # Compute the distance map between all residues (minimal distance between atoms of the chain)
    distanceMap=np.zeros([len(residues),len(residues)],dtype=float)
    catPdbSeq=''
    for idx1,res1 in enumerate(residues):
        atoms1 = np.array([r.get_coord() for r in res1.get_atoms()])
        catPdbSeq+=Bio.PDB.Polypeptide.three_to_one(res1.resname)
        for idx2,res2 in enumerate(residues):
            atoms2 = np.array([r.get_coord() for r in res2.get_atoms()])
            atomDist = scipy.spatial.distance.cdist(atoms1,atoms2)
            distanceMap[idx1,idx2]=atomDist.min()

    
    ### Align the pdb sequence to its reference mapped sequence
    pdbSeq = pdbToFasta(pdbFile,chainId,None)
    origSeq = list(Bio.SeqIO.parse(sequence1,'fasta'))[0].seq
    ali = pairwise2.align.localxx(origSeq,pdbSeq)[0]
    aliIndexes = getAlignmentIndexes(ali[1],ali[0])

    if seq1Mapping!="None":
        with open(seq1Mapping,'r') as f:
            refMapping = [int(l) for l in f]
    else:
        refMapping = np.arange(len(origSeq))
        
    ### Align the map to the refernce mapping
    dm1 = np.inf*np.ones(())

    # Save output map
    if mapFile!='None':
        np.savetxt(mapFile,dm,fmt='%.2f')

    return dm
    
def getAlignmentIndexes(seq1,seq2):
    """ To be commented

    """

    gap="-"
    
    if len(seq1)!=len(seq2):
        logging.error("Error: Sequences must be of same length if aligned.")
        return

    seq2Map=-np.ones(len(seq2),dtype=int)   
    posCounter=0
    for i,c in enumerate(seq2):
        if c is not gap:
            seq2Map[i]=posCounter
            posCounter+=1
            
    indexes=[]
    for i,c in enumerate(seq1):
        if c is not gap:
            indexes.append(seq2Map[i])

    return np.asarray(indexes,dtype=int)


def mergeDomains(inFasta, outFasta):
    """ Combines domains hits into single sequences based on sequence identifiers. 
        If multiple domains (with the same sequence id) are presenent as different sequences in the MSA, stitch them together as a new sequence.

    Keyword Arguments:
        inFasta (str): The input MSA in fasta format. The sequence header must have the following form >XX|Id|YYYYY

        ouFasta (str): The output name for the recomposed MSA. 

    """

    msa=Bio.SeqIO.to_dict(Bio.SeqIO.parse(inFasta,'fasta'))
    
    N = len(msa[msa.keys()[0]].seq)

    uniqIds = set([key.split('|')[1] for key in msa.keys()])
    mergedMSA = []
    ff = open('pp','w')
    for n,uniqId in enumerate(uniqIds):
        ids = [fullId for fullId in msa.keys() if uniqId in fullId]
        seqs = [msa[i] for i in ids]

        tmpSeq= ["-"]*N
        for seq in seqs:
            tmpSeq = [c if tmpSeq[ind]=='-' else tmpSeq[ind] 
                 for ind,c in enumerate(seq.seq)]

        mergedSeq = Bio.SeqRecord.SeqRecord(Bio.Seq.Seq("".join(tmpSeq)),id=uniqId)
        mergedMSA.append(mergedSeq)
        logging.info( "{}/{}".format(n,len(uniqIds)))

    ff.close()
    with open(outFasta,"w") as f:
        Bio.SeqIO.write(mergedMSA,f,"fasta")



def fastaToMatrix(fastaFile):
    """ Loads a fasta file into a numerical matrix with amino-acids from 0 to 20.

    Keyword Arguments:
        fastaFile (str): Name of the input fasta file

    Returns:
        msa (ndarray): A numpy array containing the MSA in numerical format
    """
    
    msa=Bio.SeqIO.parse(fastaFile,'fasta')
    seqs=[list(str(s.seq)) for s in msa]   
    matrix= np.asarray([[aaDict[aa] for aa in seq] for seq in seqs])

    msa=Bio.SeqIO.parse(fastaFile,'fasta')
    ids=np.asarray([s.description for s in msa],dtype=None)
    return matrix,ids

def matrixToFasta(msa,fastaFile,ids=None):
    """ Saves a MSA in numerical matrix format to a fasta file

    Keyword Arguments:
        msa (ndarray): A numpy array containing the MSA in numerical format

        fastaFile (str): Name of the output fasta file

        ids (list): List of ids to be used in the fasta header of each sequence in msa.
    """

    tmp=[]
    for i,line in enumerate(msa):
        seq =  ''.join([invAaDict[int(aa)] for aa in line])
        if ids is not None:
            tmp.append(Bio.SeqRecord.SeqRecord(Bio.Seq.Seq(seq),id=ids[i],name="",description=""))
        else:
            tmp.append(Bio.SeqRecord.SeqRecord(Bio.Seq.Seq(seq),id=str(i),name=str(i),description=str(i)))
    Bio.SeqIO.write(tmp,fastaFile,"fasta")
    
def shuffleMSA(msa):
    """ Shuffles the columns of the input MSA and returns a MSA conserving only the positional conservations.

    Keyword Arguments:
        msa (ndarray): A numpy array containing the MSA in numerical format

    Returns:
        shuffledMSA (ndarray): A numpy array containing the reshuffled MSA in numerical format

    """

    shuffledMSA = np.zeros(msa.shape,dtype=int)

    for i in range(msa.shape[1]):
        rndIs = np.random.permutation(msa.shape[0])
        shuffledMSA[:,i] = msa[rndIs,i]

    return shuffledMSA


def countSequencesInMSA(msaFile):
    """ Counts the number of sequences in a fasta file
    
    Keyword argumnts:
        msaFile (str): Fasta file containing the MSA

    Returns:
       N (int): The number of sequences in te MSA
    """
    N=0
    with open(msaFile,'r') as f:
        for l in f:
            if l[0]==">":
                N+=1
    return N

def compareMaps(mapFiles,combinedMap):
   """ Combines two aligned distance maps in one, putting them in the two different triangular parts of the resulting map.

   Keyword Arguments:
       mapFiles (list): List of aligned distance maps (as returned by pdbMap) to be combined.

       combinedMap (str): The output name where the combined maps is saved.
   """

   # Load all maps to be combined
   if len(mapFiles)!=2:
     raise("Error: you can only compare 2 maps")
   maps=[]
   for map in mapFiles:
       maps.append(np.loadtxt(map))

   compMap=np.tril(maps[1])+np.triu(maps[0 ])

   np.savetxt(combinedMap,compMap,fmt='%.2f')

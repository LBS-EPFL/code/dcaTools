import numpy as np
import scipy.spatial.distance
import pandas as pd
import Bio.SeqIO
import matplotlib.pyplot as plt

def getSize(dcaFile):
  """Returns the size N of the NxN DCA map.

  Keyword arguments:
      dcaFile (str): The file containing the DCA contacts. The file must only contain the upper triangular part 
                           of the DCA score matrix, ordered in linear form, i.e. the contacts are ordered as 
                           S(1,2),S(1,3),...,S(1,N),S(2,3),S(2,4),...,S(N-1,N)        
  Returns:
      N (int): The size N of the NxN DCA map.
  """

  dca=scipy.spatial.distance.squareform(np.loadtxt(dcaFile))
  return dca.shape[0]

def extractTopContacts(dcaFile,numTopContacts,diagIgnore=4):
  """Extract the top N ranked DCA contacts.

  Keyword arguments:
      dcaFile (str): The file containing the DCA contacts. The file must only contain the upper triangular part 
                         of the DCA score matrix, ordered in linear form, i.e. the contacts are ordered as 
                         S(1,2),S(1,3),...,S(1,N),S(2,3),S(2,4),...,S(N-1,N)
    
      numTopContacts (int): The number of top ranked contacts to return. 

      diagIgnore (int): The number of diagonal bands to be ignored, starting from the center. 
                      

  Returns:
      topContacts (ndarray) : A 2D numpy array of dimension (numTopContacts,2), 
                                    containing the (i,j) pairs of the extracted contacts. 

      sortedScores (ndarray): A 1D numpy array containing the sorted scores of the numTopContacts.
  """
   
    # Load the data from file and put in symmetric matrix form
  try:
    dca=scipy.spatial.distance.squareform(np.loadtxt(dcaFile))
  except:
    dca=np.loadtxt(dcaFile)
    dca-=np.diag(np.diag(dca))
    # Ignore the diagIgnore first diagonal bands
  N=dca.shape[0]
  for i in range(1,diagIgnore+1):
    dca=dca-1e5*np.diag(np.ones(N-i),k=i)
    dca=dca-1e5*np.diag(np.ones(N-i),k=-i)
  # Extract the top ranked contacts
  sortedScores = -np.sort(scipy.spatial.distance.squareform(-dca))
  dcaContacts=np.argwhere(dca>sortedScores[numTopContacts])
  contactRanks=np.argsort(-dca[dcaContacts[:,0],dcaContacts[:,1]])
  dcaContacts = dcaContacts[contactRanks,:]
  return dcaContacts,sortedScores

def extractTopContacts3d(tensor,numTopContacts,diagIgnore=4):
    for r in range(tensor.shape[0]):
        first=max(r-diagIgnore-1,0)
        last=min(r+diagIgnore+1,tensor.shape[0])
        tensor[r,first:last,:]-=1e5
        tensor[r,:,first:last]-=1e5
        tensor[:,r,first:last]-=1e5
    sortedScores=-np.sort(-tensor.flatten())[::6]
    dcaContacts=np.argwhere(tensor>sortedScores[numTopContacts])
    contactRanks=np.argsort(-tensor[dcaContacts[:,0],dcaContacts[:,1],dcaContacts[:,2]])
    dcaContacts = np.sort(dcaContacts[contactRanks,:],axis=1)[::6]
    return dcaContacts,sortedScores



def extractInterContacts(dcaFile,N1,Beff,cutoff=0.8):
  """ Extracts the inter-protein DCA contacts using the Hopf2014 criterion 
    
  Keyword arguments:
      dcaFile (str): The file containing the DCA contacts. The file must only contain the upper triangular part 
                         of the DCA score matrix, ordered in linear form, i.e. the contacts are ordered as 
                         S(1,2),S(1,3),...,S(1,N),S(2,3),S(2,4),...,S(N-1,N)

      N1 (int): The length of the first  protein in the alignmnet.

      Beff (double): The number of effective sequences after identity reweighting.

      cutoff (double): The cutoff on the score over which inter-protein contacts are considered.


  Returns:
          topContacts (ndarray) : A 2D numpy array of dimension (numTopContacts,2), 
                                    containing the (i,j) pairs of the extracted contacts. 

          sortedScores (ndarray): A 1D numpy array containing the sorted ***original*** DCA scores of the numTopContacts.
  """

  dca=scipy.spatial.distance.squareform(np.loadtxt(dcaFile))
  N=dca.shape[0]
  inter = dca[:N1,N1:]

  Qraw = inter/np.abs(inter.min())
  Qij = Qraw/(1.+np.sqrt(N/float(Beff)))
  contacts = np.argwhere(Qij>=cutoff)
  scores = Qij[contacts[:,0],contacts[:,1]]
  return contacts,scores
    
def combineMaps(mapFiles):
  """ Combines multiple mapped structural contact maps in a single one. The combination is based on an element-wise minimum.

  Keyword Arguments:
      mapFiles (list): List of aligned distance maps (as returned by pdbMap) to be combined. 

      combinedMap (str): The output name where the combined maps is saved.
  """

  # Load all maps to be combined
  maps=[]
  for map in mapFiles:
    maps.append(np.loadtxt(map))
       
  minMap=np.amin(np.array(maps),axis=0)
  return minMap

def computeMSAPottsEnergies(h,J,fastaFile):
    """ Computes the Potts energies for all sequences in the MSA.

    Keyword Arguments:
        h (ndarray): 1-D array containing the potts model local biases h_i(A) in the following order: h_0(0),h_0(1),...,h_0(q),h_1(0),...,h_N(q)

        J (ndarray): 1-D array containing the potts model couplings J_ij(A,B) in the following order J_01(0,0),J_01(0,1),...,J_01(0,q),J_01(1,0),...,J_02(0,0),...,J_N-1,N(q,q)

        fastaFile (str): The file containing the MSA in fasta format.
    
    Returns:
        energies (ndarray):
    """

    aaDict={'-':0, 'X':0,'Z':0,'B':0, 'A':1, 'C':2, 'D':3, 'E':4, 'F':5, 'G':6, 'H':7, 'I':8, 'K':9, 'L':10, 'M':11,
            'N':12, 'P':13, 'Q':14, 'R':15, 'S':16, 'T':17, 'V':18, 'W':19, 'Y':20}
    msa=list(Bio.SeqIO.parse(fastaFile,'fasta'))

    energies = np.zeros((len(msa),3))
    N = len(msa[0].seq)
    q= int(h.shape[0]/N)
    for k,seq in enumerate(msa):
        numSeq=[aaDict[p] for p in seq.seq]
        energies[k,1]=h[q*np.arange(N)+numSeq].sum()
        for i in range(N-1):
            Jind = ((i*N -(i+2)*(i+1)/2+np.arange((i+1),N,dtype=int))*q*q + q*numSeq[i]+numSeq[(i+1):N]).astype(int)
            energies[k,2]+=J[Jind].sum()

    energies[:,0]= energies[:,1]+energies[:,2]
    return energies

def readPRMfile(prmFile,N,q=21):
    """ Reads Potts model parameters in prm format and returns two numpy arrays containing the fields and couplings.
    
    Keyword Arguments:
         prmFile (str): The file containing the local biases h_i(A) and couplings J_ij(A,B) 
                        of the potts model in ASCII format.

    Returns:
        h (ndarray): 1-D array containing the potts model local biases h_i(A) in the following order: 
                     h_0(0),h_0(1),...,h_0(q),h_1(0),...,h_N(q)
    
        J (ndarray): 1-D array containing the potts model couplings J_ij(A,B) in the following order: 
                     J_01(0,0),J_01(0,1),...,J_01(0,q),J_01(1,0),...,J_02(0,0),...,J_N-1,N(q,q)
    """

    hJ=np.fromfile("dca_fullParams.dat")
    h=hJ[:N*q].ravel()
    J=hJ[N*q:].ravel()
    return h,J

def writePRMFile(h,J,prmFile):
    """ Saves the Potts Model parameters in h,J in prm format.

    Keyword Arguments:
        h (ndarray): 1-D array containing the potts model local biases h_i(A) in the following order: 
                     h_0(0),h_0(1),...,h_0(q),h_1(0),...,h_N(q)

        J (ndarray): 1-D array containing the potts model couplings J_ij(A,B) in the following order: 
                     J_01(0,0),J_01(0,1),...,J_01(0,q),J_01(1,0),...,J_02(0,0),...,J_N-1,N(q,q)

        prmFile (str): The output file containing the local biases h_i(A) and couplings J_ij(A,B) 
                       of the Potts model in ASCII format.
    """
    q = 21
    N = h.shape[0]/q

    with open(prmFile,'w') as f:
        f.write('protein   '+str(N)+'   '+str(q)+'\n')
        for i in range(N):
            for A in range(q):
                f.write('    '+str(i+1)+'    '+str(A+1)+'    '+str(h[q*i+A])+'\n')
        c=0
        for i in range(N):
            for j in range(i+1,N):
                for A in range(q):
                    for B in range(q):
                        f.write('    '+str(i+1)+'    '+str(j+1)+'    '+str(A+1)+'    '+str(B+1)
                                +'    '+str(J[c])+'\n')
                        c+=1
                        
def prmToIsingGauge(h,J):
    """ Shifts the Potts model parameters h and J to the zero-sum (i.e. Ising) gauge such that the 
        following conditions are satisfied:
    
        \sum_A[h_i(A)] = 0    \forall i
  
        \sum_A[J_ij(A,B)] = 0 \forall i,j,B

        \sum_B[J_ij(A,B)] = 0 \forall i,j,A   
    
        Keyword Arguments:
            h (ndarray): 1-D array containing the potts model local biases h_i(A) in the following order:   
                     h_0(0),h_0(1),...,h_0(q),h_1(0),...,h_N(q)                                                   
                                                                                                                  
            J (ndarray): 1-D array containing the potts model couplings J_ij(A,B) in the following order:
                     J_01(0,0),J_01(0,1),...,J_01(0,q),J_01(1,0),...,J_02(0,0),...,J_N-1,N(q,q)
     
       Returns:
            h0 (ndarray): Array of same dimensions as h containing the zero-sum biases

            J0 (ndarray): Array of same dimensions as J containig the zero-sum couplings
    """

    q=21
    N=h.shape[0]/q
    
    h0 = np.zeros(h.shape)
    J0 = np.zeros(J.shape)

    # Shift couplings to zero-sum gauge
    c=0
    for i in range(N):
        h0[i*q:(i+1)*q] += h[i*q:(i+1)*q]
        for j in range(i+1,N):
            jij = np.reshape(J[c:c+q*q],(q,q))
            j0 = jij - jij.mean(axis=0,keepdims=True) - jij.mean(axis=1,keepdims=True) + jij.mean()
            J0[c:c+q*q]=np.reshape(j0,(q*q))
            c+=q*q
            
            h0[i*q:(i+1)*q] += jij.mean(axis=1)
            h0[j*q:(j+1)*q] += jij.mean(axis=0)

    # Shift fields to zero-sum gauge
    for i in range(N):
        h0[i*q:(i+1)*q]-=h0[i*q:(i+1)*q].mean()

    return h0,J0

def dummyMap(N,outFile):
    """ Creates a NxN empty distance map for rapid visualization of DCA results. Only the diagonal has contacts (dist=1) while the rest has distances of 100.

        Keyword Arguments:
            N (int): The number of residues in the dummy map

            outFile (str): The name of the output file
    """

    dm = 100*np.ones((N,N))
    dm = dm - np.diag(np.diag(dm)) + np.eye(N)

    np.savetxt(outFile,dm,delimiter=' ',fmt='%.2f')

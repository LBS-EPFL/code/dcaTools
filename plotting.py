import dca
import Bio
import scipy
import numpy as np
import sequenceHandler as sh
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.colors import LogNorm
from scipy.spatial.distance import squareform
from sklearn.decomposition import TruncatedSVD
from matplotlib.colors import LinearSegmentedColormap
from matplotlib import collections  as mc

styles={ 'display':{'contactmap':'0.55','pred_true':'lime','pred_false':'r','transparent':False,'distancemap_background':False,'triplets_color':'blue'},
         'paper':{'contactmap':'0.75','pred_true':'blue','pred_false':'orange','transparent':True,'distancemap_background':False,'triplets_color':'blue'},
         'distances':{'contactmap':'0.75','pred_true':'lime','pred_false':'r','transparent':True,'distancemap_background':True,'triplets_color':'blue'},
       }

def displayTopContacts(dcaFile,Ntop,hmm,pdb,minSeqSeparation=4,showas='spheres',shownonbonded=False,usebonds=True,usecolor=True):
  if ':' in pdb:
    pdb,chain=pdb.split(':')
    chain=[ch for ch in chain]
  else:
    chain=['A']

  seqs=[]
  mapIdx=[]
  for ch in chain:
    seqs.append(sh.pdbToFasta(pdb,ch))
    mapIdx.append(sh.alignSequenceToHMM(seqs[-1],hmm))
  dcaContacts,_=dca.extractTopContacts(dcaFile,Ntop,minSeqSeparation)

  colors=['red', 'blue', 'deepteal', 'slate', 'brightorange', 'deepolive', 'cyan', 'orange', 'firebrick', 'deeppurple', 'yellow', 'lightmagenta', 'green', 'purple', 'forest', 'brown', 'yelloworange']
  chaincolors=["wheat","palegreen","lightblue","paleyellow","lightpink","palecyan","lightorange","bluewhite"]
  pdbIdx={ch:[] for ch in chain}
  with open(pdb,'r') as infile:
    for line in infile:
      if len(line)==0:
        continue
      tok=line.split()
      if len(tok)==0:
        continue
      if tok[0]=="ENDMDL":
        break
      if tok[0]!="ATOM":
        continue
      ch=line[21]
      if ch not in chain:
        continue
      resid=int(line[22:26])
      if len(pdbIdx[ch])==0:
        pdbIdx[ch].append(resid)
      elif resid!=pdbIdx[ch][-1]:
        pdbIdx[ch].append(resid)

  import pymol
  from pymol import cmd
  pymol.pymol_argv = ['pymol','-q']
  pymol.finish_launching()
  cmd.load(pdb)
  for chidx,ch in enumerate(chain):
    cmd.color(chaincolors[chidx],"chain {}".format(ch))

  for idx,(idx1,idx2) in enumerate(dcaContacts):
    selection=[]
    connection=[]
    for chidx,ch in enumerate(chain):
      try:
        res1=pdbIdx[ch][mapIdx[chidx][idx1]]
        res2=pdbIdx[ch][mapIdx[chidx][idx2]]

      except:
        continue
      print(ch,res1,res2,seqs[chidx][mapIdx[chidx][idx1]],seqs[chidx][mapIdx[chidx][idx2]])
      connection.append( (ch,res1,res2) )
      selection.append("(chain {} and resi {}+{})".format(ch,res1,res2))
    if len(selection):
      selection=" or ".join(selection)
      cmd.select( "cnt{}".format(idx), selection )
      if usecolor is True:
        cmd.color(colors[idx%len(colors)],"cnt{}".format(idx))
      cmd.show_as(showas,"cnt{}".format(idx))
      cmd.show('sticks',"cnt{}".format(idx))
    if len(connection) and usebonds is True:
      for cnt in connection:
        cmd.bond("chain {} and name ca and resi {}".format(cnt[0],cnt[1]), "chain {} and name ca and resi {}".format(cnt[0],cnt[2]))
  
  if shownonbonded is False:
    cmd.hide('nonbonded')

def plotTopContacts(dcaFile,Ntop,triplets=None,NtopTriplets=5,pdbMap=None,contactThreshold=8.5,minSeqSeparation=4,style='display',save=None, hmm=None, seqfile=None, columns=None, first=None):
  """ Plots the top N ranked DCA predictions overlaip on the structual contact map

  Keword Arguments:
    dcaFile (str): The file containing the DCA contacts. The file must only contain the upper triangular part 
                   of the DCA score matrix, ordered in linear form, i.e. the contacts are ordered as 
                   S(1,2),S(1,3),...,S(1,N),S(2,3),S(2,4),...,S(N-1,N)

    Ntop (int)   : The number of top ranked DCA contacts to plot. 
    pdbMap (str) : (Optional) The aligned distance map file, as output by mapPDB.
    contactThreshold (float): (Optional) The threshold distance in pdbMap defining a structural contact.
    minSeqSeparation (int): Minimum number of separation along the sequence (in the alignment) below which contacts are not extracted (default 4)
    style (str): Define the plotting style to be used
    save (str): If provided, the figure will be saved to a file and not displayed
  """
  def getTriangles(vertices):
    triangles=[]
    for vert in vertices:
        triangles.append([ (vert[0],vert[1]),(vert[1],vert[2]) ] )
        triangles.append([ (vert[0],vert[2]),(vert[0],vert[1]) ] )
        triangles.append([ (vert[1],vert[2]),(vert[0],vert[2]) ] )
    return triangles

  style=styles[style]
  # Plot structural contacts
  if pdbMap is not None:
    dm=np.loadtxt(pdbMap,dtype=float)
  else:
    try:
      dm=squareform(np.loadtxt(dcaFile))
    except:
      dm=np.loadtxt(dcaFile)
    dm[:]=np.inf

  
  if seqfile is not None:
    seq=str(list(Bio.SeqIO.parse(seqfile,'fasta'))[0].seq)
  else:
    seq="X"*dm.shape[0]

  if hmm is not None:
    mapIndexes=sh.alignSequenceToHMM(seq,hmm)
  else:
    mapIndexes=range(dm.shape[0])
    

  highlight=[]
  if columns is not None:
    for col in columns:
      try:
        highlight.append(list(mapIndexes).index(col))
      except:
        continue
  fig = plt.figure(figsize=(6,6))
  ax=fig.add_subplot(111)
  annot = ax.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
                    bbox=dict(boxstyle="round", fc="w"),
                    arrowprops=dict(arrowstyle="->"))
  annot.set_visible(False)
  
  pdbContacts=np.argwhere(dm<=contactThreshold)

  # Overlay DCA predictions
  dcaContacts,_=dca.extractTopContacts(dcaFile,Ntop,minSeqSeparation)
  dcaColors=dm[dcaContacts[:,0],dcaContacts[:,1]]<contactThreshold
  dcaColors=[style['pred_true'] if col else style['pred_false'] for col in dcaColors]
#  names=["({}{},{}{}) ".format(seq[mapIndexes[d[0]]],mapIndexes[d[0]]+1,seq[mapIndexes[d[1]]],mapIndexes[d[1]]+1) for d in dcaContacts.tolist() if (mapIndexes[d[0]]>0 and mapIndexes[d[1]]>0) ]

  # Handle the figure
  if not style['distancemap_background']:
    plt.scatter(pdbContacts[:,0],pdbContacts[:,1],s=15,color=style['contactmap'],alpha=0.4)
  else:
#    plt.imshow(1./np.power((dm-np.min(dm)+1),0.3),cmap='Greys')
    im=ax.imshow(dm.T,cmap='gray',vmin=0,vmax=20.)
    cbar=plt.colorbar(im,fraction=0.046, pad=0.04)
    cbar.ax.set_ylabel('Distance in Angstrom', rotation=90)
    cbar.ax.set_yticklabels(['0.0','2.5','5.0','7.5','10.0','12.5','15.0','17.5','> 20.0'])
  sc=plt.scatter(dcaContacts[:,0],dcaContacts[:,1],s=8,color=dcaColors)
  
  if triplets is not None:
    tensor3d=np.load(triplets)
    cnt3d,sortscores3d=dca.extractTopContacts3d(tensor3d,NtopTriplets)
    print(cnt3d+1)
    lc=mc.LineCollection( getTriangles(cnt3d),linewidths=1,colors=style['triplets_color'] )
    ax.add_collection(lc)

  if len(highlight):
    H=np.zeros(dm.shape)
    for col in highlight:
      H[col,:]=1
      H[:,col]=1
    im=ax.imshow(H,cmap='Blues',vmin=0,vmax=1.,alpha=0.6)


  
  plt.xlim([-2,dm.shape[0]+2])
  plt.ylim([-2,dm.shape[1]+2])
  plt.grid()
  
  plt.gca().invert_yaxis()
  plt.gca().set_aspect('equal')
  if first is not None:
    locs,labels=plt.xticks()
    labels=[str(int(l+first)) for l in locs]
    plt.xticks(locs[1:-1],labels[1:-1])
    plt.yticks(locs[1:-1],labels[1:-1])
  plt.tight_layout(pad=0., w_pad=0., h_pad=0.)
  ax.set_axisbelow(True)
  


  ranks={}
  idx=0
  for cnt in dcaContacts:
    if cnt[0]<cnt[1]:
      idx+=1
      ranks[tuple(cnt)]=idx
  
  def hover(event):
    vis = annot.get_visible()
    if event.inaxes == ax:
      cont, ind = sc.contains(event)
      if cont:
        update_annot(ind)
        annot.set_visible(True)
        fig.canvas.draw_idle()
      else:
        if vis:
          annot.set_visible(False)
          fig.canvas.draw_idle()
          
  def update_annot(ind):
    pos_s=[ [int(d[0]),int(d[1])] for d in sc.get_offsets()]
    pos = [pos_s[ind["ind"][0]]]
    annot.xy = pos[0]
    
    seqX='X'
    idxX=pos[0][0]+1
    seqY='X'
    idxY=pos[0][1]+1
    rank=ranks[(min(idxX-1,idxY-1),max(idxX-1,idxY-1))]
    d=pos[0]
    try:
      seqX=seq[mapIndexes[d[0]]]
      idxX=mapIndexes[d[0]]+1
    except:
      pass
    try:
      seqY=seq[mapIndexes[d[1]]]
      idxY=mapIndexes[d[1]]+1
    except:
      pass
    text="{}: ({}{},{}{})".format(rank,seqX,idxX,seqY,idxY)
#    text = "{}".format(" ".join([names[n] for n in ind["ind"]]))
    annot.set_text(text)
    annot.get_bbox_patch().set_facecolor(((dcaColors[ind["ind"][0]])))
    annot.get_bbox_patch().set_alpha(0.4)
  
  if save is None:
    fig.canvas.mpl_connect("motion_notify_event", hover)
    plt.show()
  else:
    plt.savefig(save,dpi=300,transparent=style['transparent'])

def listTopContacts(dcaFile,Ntop,hmmFile,seqFile,minSeqSeparation=4):
  """ List the N top ranked DCA predicted contacts. If an optional reference sequence and the mapping hmm are passed,
      the predicted contacts are reported in the reference sequence indexing.

    Keyword Arguments:
      dcaFile (str): The file containing the DCA contacts. The file must only contain the upper triangular part 
                     of the DCA score matrix, ordered in linear form, i.e. the contacts are ordered as 
                     S(1,2),S(1,3),...,S(1,N),S(2,3),S(2,4),...,S(N-1,N)

      Ntop (int)   : Ntop (int)   : The number of top ranked DCA contacts to plot. 

      hmmFile (str): An optional hmmFile defining the family. If this is passed together with seqFile, the contacts indexes are 
                     both returned in MSA numbering and in the numbering of the reference sequence in seqFile.

      seqFile (str): Optional reference sequence file. If provided together with the family hmmFile, the contacts indexes are 
                     both returned in MSA numbering and in the numbering of the reference sequence in seqFile.

      minSeqSeparation (int): Minimum number of separation along the sequence (in the alignment) below which contacts are not extracted (default 4)
  """
    
  # Extract DCA predictions
  dcaContacts,sortedScores=dca.extractTopContacts(dcaFile,Ntop,minSeqSeparation)
  dcaMatrix=squareform(np.loadtxt(dcaFile))
  N=dcaMatrix.shape[0]

  # Map contacts coordinates to reference sequence (Optional)
  if(hmmFile):
    refSequence=str(list(Bio.SeqIO.parse(seqFile,'fasta'))[0].seq)
    mapIndexes=sh.alignSequenceToHMM(refSequence,hmmFile)
  else:
    mapIndexes=range(N)

  for i,index in enumerate(mapIndexes):
    if index<0:
      mapIndexes[i]=-1
    
  # Display DCA predicted contacts
  header=['i','j','i_map','j_map','rank']
  data=[]
  for contact in dcaContacts:
    if contact[0]<contact[1]:
      rank=np.argwhere(sortedScores>=dcaMatrix[contact[0],contact[1]])[-1,0]+1
      if hmmFile:
        mapAA1=refSequence[mapIndexes[contact[0]]]+str(mapIndexes[contact[0]]+1),
        mapAA2=refSequence[mapIndexes[contact[1]]]+str(mapIndexes[contact[1]]+1),
        if mapIndexes[contact[0]]==-1:
          mapAA1=['--']
        if mapIndexes[contact[1]]==-1:
          mapAA2=['--']
        data.append([contact[0]+1,contact[1]+1,mapAA1[0],mapAA2[0],rank])
      else:
        data.append([contact[0]+1,contact[1]+1,'--','--',rank])
  return header,data
  

def listInterfaceContacts(dcaFile,Beff,N1=None,hmmFile1=None,hmmFile2=None,seqFile1=None,seqFile2=None,cutoff=0.8):
  """ List the predicted inter-protein interface contacts as defined by the Hopf2014 criterion.
      If optional sequence files and hmm mapping files are provide, the predicted contacts are reported in the reference sequeces indexing.

   Keyword Arguments:
      dcaFile (str) : The file containing the DCA contacts. The file must only contain the upper triangular part 
                         of the DCA score matrix, ordered in linear form, i.e. the contacts are ordered as 
                         S(1,2),S(1,3),...,S(1,N),S(2,3),S(2,4),...,S(N-1,N)

      N1 (int)      : The length of the first  protein in the alignmnet.

      Beff (double) : The number of effective sequences after identity reweighting.s
      
      hmmFile1 (str): An optional hmmFile defining the family 1. If this is passed together with seqFile1, the contacts indexes are 
                     both returned in MSA numbering and in the numbering of the reference sequence in seqFile1.

      hmmFile2 (str): An optional hmmFile defining the family 2. If this is passed together with seqFile2, the contacts indexes are 
                     both returned in MSA numbering and in the numbering of the reference sequence in seqFile2.

      seqFile1 (str): Optional reference sequence file 1. If provided together with the family hmmFile1, the contacts indexes are 
                     both returned in MSA numbering and in the numbering of the reference sequence in seqFile1.

      seqFile2 (str): Optional reference sequence file 2. If provided together with the family hmmFile2, the contacts indexes are 
                     both returned in MSA numbering and in the numbering of the reference sequence in seqFile2.

      cutoff (double): The cutoff on the score over which inter-protein contacts are considered.
  """

#  contacts = dca.extractInterContacts(dcaFile,N1,Beff,cutoff)
  is_seq1 = hmmFile1 is not None and seqFile1 is not None
  is_seq2 = hmmFile2 is not None and seqFile2 is not None

  if not is_seq1 and not is_seq2 and not N1:
    raise RuntimeError("You must provide N1 or at list one sequence+hmm")

  Ntot = dca.getSize(dcaFile)
  
  mapIndexes1=None
  mapIndexes2=None
  refSeq1=None
  refSeq2=None
  if is_seq1:
    refSeq1=str(list(Bio.SeqIO.parse(seqFile1,'fasta'))[0].seq)
    mapIndexes1=sh.alignSequenceToHMM(refSeq1,hmmFile1)
    if N1 is not None:
      if mapIndexes1.shape[0]!=N1:
        raise RuntimeError("N1 does not match the length of the mapped sequence: {}!={}".format(N1,mapIndex1.shape[0]))
    else:
      N1=mapIndex1.shape[0]
    
  if is_seq2:
    refSeq2=str(list(Bio.SeqIO.parse(seqFile2,'fasta'))[0].seq)
    mapIndexes2=sh.alignSequenceToHMM(refSeq2,hmmFile2)
    if N1 is not None:
      if mapIndexes2.shape[0]!=(Ntot-N1):
        raise RuntimeError("Dca file does not match the length of the mapped sequences: dca_size: {}, seq1: {}, seq2: {}".format(Ntot,N1,mapIndexes2.shape[0]))
    else:
      N1 = Ntot-mapIndexes2.shape[0]
  N2 = Ntot-N1
  if mapIndexes1 is None:  
    mapIndexes1=range(N1)
  if mapIndexes2 is None:
    mapIndexes2=range(N2)

  for i,index in enumerate(mapIndexes1):
    if index<0:
      mapIndexes1[i]=-1
    

  for i,index in enumerate(mapIndexes2):
    if index<0:
      mapIndexes2[i]=-1

  contacts=dca.extractInterContacts(dcaFile,N1,Beff,cutoff)

  return contacts,(refSeq1,mapIndexes1),(refSeq2,mapIndexes2)



def plotTPrates(pdbMap,dcaFiles,Nmax,contactThreshold):
  """ Plots the precitions curves for DCA predictions
  Keyword Arguments:
    pdbMap (str): The aligned distance map file, as output by mapPDB.

    dcaFiles (list): List of file containing DCA contacts. Each file must only contain the upper triangular part 
                     of the DCA score matrix, ordered in linear form, i.e. the contacts are ordered as 
                     S(1,2),S(1,3),...,S(1,N),S(2,3),S(2,4),...,S(N-1,N)

    Nmax (int): Maximum number of predicted contacts to report for the precision curves

      contactThreshold (float): The threshold defining a structural contact. Contacts are computed between heavy-atoms.
  """

  # Load all DCA scores
  dcaScores=[]
  for dcaScore in dcaFiles:
    dcaScores.append(np.loadtxt(dcaScore))
   
  # Get structural contacts
  dm=np.loadtxt(pdbMap,dtype=float)
  pdbContacts=np.argwhere(dm<=contactThreshold)

  for score in dcaScores:
    # Compute precision for N in [1:Nmax]
    dca=scipy.spatial.distance.squareform(score)
    for i in range(5):
      dca=dca-np.diag(np.diag(dca,k=i),k=i)
      dca=dca-np.diag(np.diag(dca,k=-i),k=-i)
    sortedScores=-np.sort(scipy.spatial.distance.squareform(-dca),)
            
    precision=np.zeros(Nmax)
    for N in np.arange(Nmax):
      dcaContacts=np.argwhere(dca>sortedScores[N+1])
      precision[N]=(dm[dcaContacts[:,0],dcaContacts[:,1]]<contactThreshold).mean()
                
      # Plot the precision curve
    plt.plot(np.arange(Nmax)/float(dm.shape[0]),precision)

  # Handle the figure
  plt.ylim(0,1.05)
  plt.grid()
  plt.xlabel('$N_{Pred}/N$',fontsize=18)
  plt.ylabel('Precision',fontsize=18)
  plt.show()


def plotDistDistribution(pdbMap,dcaFile,Ntops):
    """ Plots the distribution of the distances of predicted DCA contacts.

    Keyword Arguments:
        pdbMap (str): The aligned distance map file, as output by mapPDB.

        dcaFile (str): The file containing the DCA contacts. The file must only contain the upper triangular part 
                           of the DCA score matrix, ordered in linear form, i.e. the contacts are ordered as 
                           S(1,2),S(1,3),...,S(1,N),S(2,3),S(2,4),...,S(N-1,N)

        Ntops (list): List of number of top ranked DCA predictions. A distance histogram is plotted for each value.
    """

    # Apply short-range filter (i.e. ignore i-i+4 contacts)
    dm=np.loadtxt(pdbMap,dtype=float)
    for i in range(5):
        dm=dm-np.diag(np.diag(dm,k=i),k=i)
        dm=dm-np.diag(np.diag(dm,k=-i),k=-i)
        
    # Compute PDB distances
    pdbDistances=np.triu(dm).flatten()
    pdbDistances=pdbDistances[np.isfinite(pdbDistances)]
    pdbDistances=pdbDistances[pdbDistances>0]
    pdbHist,pdbBins=np.histogram(pdbDistances,50,density=True)
    plt.plot(pdbBins[:-1],pdbHist/max(pdbHist),linewidth=3)

    legend=['PDB']
    
    # Compute DCA distances   
    for N in Ntops:
        dcaContacts,_=dca.extractTopContacts(dcaFile,N)
        dcaDistances=dm[dcaContacts[:,0],dcaContacts[:,1]]
        dcaDistances=dcaDistances[np.isfinite(dcaDistances)]
        dcaHist,dcaBins=np.histogram(dcaDistances,10,density=True)
        plt.plot(dcaBins[:-1],dcaHist/max(dcaHist),linewidth=3)
        legend.append('DCA, $N_{DCA}/N$=' + str(round(N/float(dm.shape[0]),2)))
        
    # Handle the figure
    plt.xlabel('Distance [A]',fontsize=18)
    plt.ylabel('Unnormalized Density',fontsize=18)
    plt.legend(legend)
    plt.ylim([0,1.1])
    plt.grid()
    plt.show()    



def plotFrequencyCorrelations(originalMSA, resampledMSA, computeCoFrequencies):
    """ Plots the frequencies and co-frequencies of the resampled sequences versus the original sequences.

    Keyword Arguments:
        originalMSA (str): The file containing the original MSA in fasta format

        resampledMSA (str): The file containig the aligned resampled sequences in fasta format

        computeCoFrequencies (bool): If false, only compute single-site frequencies
    """

    origMSA=sh.binarizeMSA(sh.fastaToMatrix(originalMSA)[0])
    resampledMSA=sh.binarizeMSA(sh.fastaToMatrix(resampledMSA)[0])

    fi=origMSA.mean(axis=0)
    fi_resampled=resampledMSA.mean(axis=0)

    plt.figure(1)
    plt.plot(fi,fi_resampled,'+',ms=3,c='b')
    plt.plot([0,1],[0,1],'r')
    plt.axis('equal')
    plt.xlabel('$f_i^{Data}$',fontsize=18)
    plt.ylabel('$f_i^{Model}$',fontsize=18)
    plt.xlim(0,1)
    plt.ylim(0,1)
    plt.grid()
    
    if computeCoFrequencies:
        fij=(origMSA.T.dot(origMSA)/float(origMSA.shape[0])).todense()
        fij=squareform(fij-np.diag(np.diag(fij)))
        fij_resampled=(resampledMSA.T.dot(resampledMSA)/float(resampledMSA.shape[0])).todense()
        fij_resampled=squareform(fij_resampled-np.diag(np.diag(fij_resampled)))
        plt.figure(2)
        plt.plot(fij[::1000],fij_resampled[::1000],'+',ms=3,c='b')
        plt.plot([0,1],[0,1],'r')
        plt.axis('equal')
        plt.xlabel('$f_{ij}^{Data}$',fontsize=18)
        plt.ylabel('$f_{ij}^{Model}$',fontsize=18)
        plt.xlim(0,1)
        plt.ylim(0,1)
        plt.grid()


    plt.show()


def pca(baseMSA,axes,overMSA,fast,save):
    """ Performs PCA on the baseMSA, optionally overlaying sequences in overMSA.

        Keyword Arguments:
            baseMSA (str): The file containing the base MSA in fasta format.

            axes (2-tuple): The PCA axes on which to project the data

            overMSA (str): The file containing the MSA sequences to overaly. To ignore this option, pass None"

            fast   (bool): If true, consider only a maximum of 1000 sequences to perform the PCA.

            save  (str): Optional filename to wich save the projected coordinates. If None, no coordinates are saved."
    """
    
    axes=[int(axes.split(',')[0]),int(axes.split(',')[1])]
    categorical=sh.binarizeMSA(sh.fastaToMatrix(baseMSA)[0])

    if fast: 
        categorical = categorical[0:min(1000,categorical.shape[0]),:]

    meanSeq=categorical.mean(axis=0)
    categorical=categorical-meanSeq
    svd=TruncatedSVD(n_components=max(axes)+1,algorithm="randomized", n_iter=2)
    svd=svd.fit(categorical)
    projected=svd.transform(categorical)

    plt.hexbin(projected[:,axes[0]],projected[:,axes[1]],gridsize=60,cmap='Blues',norm=LogNorm())
    
    if overMSA:
        print(overMSA)
        categoricalOver=sh.binarizeMSA(sh.fastaToMatrix(overMSA)[0])
        categoricalOver=categoricalOver-meanSeq
        projectedOver=svd.transform(categoricalOver)
        plt.scatter(projectedOver[:,axes[0]],projectedOver[:,axes[1]],7,color='r')

    plt.axis('equal')
    plt.xlabel('PC ' + str(axes[0]))
    plt.ylabel('PC ' + str(axes[1]))
    plt.grid()
    plt.show()
    plt.tight_layout()

    if save:
        np.savetxt(save,projected,delimiter=' ')
    return projected




            
def linCMap(color):
    """ Return a colormap, linearly interpolated between white and color

    Keyword Arguments:
         color (str): The color used for interpolatio in hex code.

    Returns:
         cmap (Colormap object): The colormap object in pyplot format

    """
    cmap = LinearSegmentedColormap.from_list('#FFFFFF', ['white', color])
    return cmap
